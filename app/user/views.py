from rest_framework import generics, authentication, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from user.serializers import UserSerializer, AuthTokenSerializer
from django.contrib.auth import authenticate

class CreateUserView(generics.CreateAPIView):
    """Create a new user in the system"""
    serializer_class = UserSerializer


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for user"""
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""
    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authentication user"""
        return self.request.user

def registrationView(request):

    return render(request, "user/registration.html", {})

def loginView(request):
    """ login View"""

    print(request.META.get("HTTP_AUTHORIZATION"))
    if request.method == "POST":
        user = get_user_model().objects.get(email=request.POST["email"])
        print(request.session.items())

        if user.check_password(request.POST["password"]):

            token = Token.objects.get_or_create(user=user)
            request.session["token"] = token[0].key
            request.session.save()        
    
    return render(request, "user/login.html", {})

def profileView(request):


    return render(request, "user/profile.html", {})