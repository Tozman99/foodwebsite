from core.models import Recipe
from django.forms import ModelForm


class RecipeForm(ModelForm):

    class Meta:

        model = Recipe
        fields = ["title", "price", "time_minutes"]
        